import logging

from aiohttp import web
import aiohttp_jinja2
import jinja2
from graphql.type.schema import GraphQLSchema
from graphql_server.aiohttp import GraphQLView

from alexandriabooks_core.services import ServiceRegistry, BackgroundService
from alexandriabooks_core import config

from .rest import add_cover_endpoint, add_download_endpoint, add_transcode_endpoint
from .interfaces import add_interface_endpoints
from .opds import add_opds_endpoints
from .opensearch import add_opensearch_endpoints
from .graphql.query import query_type

logger = logging.getLogger(__name__)


class WebServer(BackgroundService):
    def __init__(self, service_registry: ServiceRegistry):
        super().__init__()
        self._service_registry = service_registry
        self._routes = web.RouteTableDef()
        self._listen = config.get("web.host", "0.0.0.0")

        port_str = config.get("web.port")
        if not port_str or not port_str.isnumeric():
            raise Exception(f"Bad sharing port '{port_str}'")
        self._port = int(port_str)

    def __str__(self):
        return "Web Server"

    async def initialize(self):
        """Initialize the service."""

        @self._routes.get("/")
        async def index_page(request):
            raise web.HTTPFound("/lofi")

        add_interface_endpoints(self._routes, self._service_registry, "/api")
        add_cover_endpoint(self._routes, self._service_registry, "/api")
        add_download_endpoint(self._routes, self._service_registry, "/api")
        add_transcode_endpoint(self._routes, self._service_registry, "/api")
        add_opds_endpoints(self._routes, self._service_registry, "/api")
        add_opensearch_endpoints(self._routes, self._service_registry, "/api")

        schema = GraphQLSchema(query_type)

        self._app = web.Application()
        GraphQLView.attach(
            self._app,
            schema=schema,
            graphiql=True,
            route_path="/api/graphql",
            batch=True,
            enable_async=True,
            context={
                "service_registry": self._service_registry
            }
        )

        aiohttp_jinja2.setup(
            self._app,
            loader=jinja2.PackageLoader("alexandriabooks_apiserver", "templates"),
            enable_async=False
        )

        self._app.add_routes(self._routes)

    async def start(self):
        """Start the service."""
        logger.info(f"Starting HTTP service on {self._port}")

        self._runner = web.AppRunner(self._app)
        await self._runner.setup()
        self._site = web.TCPSite(self._runner, self._listen, self._port)
        await self._site.start()

    async def stop(self):
        """Start the service."""
        await self._site.stop()
