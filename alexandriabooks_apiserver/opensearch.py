import logging
from aiohttp import web
from alexandriabooks_core.services import ServiceRegistry
from lxml import etree

logger = logging.getLogger(__name__)


def add_opensearch_endpoints(
    routes: web.RouteTableDef, service_registry: ServiceRegistry, endpoint: str
):
    @routes.get(endpoint + "/opensearch/search.xml")
    def opds_root(request: web.Request):
        logger.debug("Serving OpenSearch definition to %s", str(request.remote))

        root = etree.Element(
            "{http://a9.com/-/spec/opensearch/1.1/}OpenSearchDescription",
            nsmap={None: "http://a9.com/-/spec/opensearch/1.1/"},
        )

        etree.SubElement(
            root, "{http://a9.com/-/spec/opensearch/1.1/}ShortName"
        ).text = "Book Search"

        etree.SubElement(
            root, "{http://a9.com/-/spec/opensearch/1.1/}Description"
        ).text = "Search books hosted on Alexandia Books."

        etree.SubElement(
            root,
            "{http://a9.com/-/spec/opensearch/1.1/}Url",
            attrib={
                "type": "text/html",
                "template": (
                    request.scheme
                    + "://"
                    + request.host
                    + "/lofi?terms={searchTerms}&offset={startIndex}&limit={count}"
                ),
                "rel": "results",
                "indexOffset": "0",
            },
        )

        etree.SubElement(
            root,
            "{http://a9.com/-/spec/opensearch/1.1/}Url",
            attrib={
                "type": "application/atom+xml;profile=opds-catalog;kind=acquisition",
                "template": (
                    request.scheme
                    + "://"
                    + request.host
                    + endpoint
                    + "/opds/books.xml?terms={searchTerms}"
                    + "&offset={startIndex}&limit={count}"
                ),
                "rel": "results",
                "indexOffset": "0",
            },
        )

        response = web.Response(status=200)
        response.content_type = "application/opensearchdescription+xml"
        response.charset = "UTF-8"
        response.body = etree.tostring(root, encoding="utf-8", pretty_print=True)
        return response
