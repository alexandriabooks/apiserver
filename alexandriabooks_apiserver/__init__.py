import logging
import typing
from alexandriabooks_core import config

from .app import WebServer

logger = logging.getLogger(__name__)
ALEXANDRIA_PLUGINS: typing.Dict[str, typing.Any] = {"BackgroundService": []}

if config.get("web.port"):
    ALEXANDRIA_PLUGINS["BackgroundService"].append(WebServer)
else:
    logger.debug("No port specified for API service, skipping initialization.")
