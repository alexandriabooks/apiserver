from alexandriabooks_core.model import BookMetadata, PersonMetadata, SeriesMetadata
import logging
import typing

from aiohttp import web

from feedgen.feed import FeedGenerator
from feedgen.ext.base import BaseExtension, BaseEntryExtension
from feedgen.util import xml_elem

from alexandriabooks_core.services import ServiceRegistry
from .utils import activate_profile_from_request, get_book_title, url_quote

logger = logging.getLogger(__name__)


class OpdsFeedExtension(BaseExtension):
    OPDS_NS = "http://opds-spec.org/2010/catalog"

    def __init__(self):
        super().__init__()

    def extend_ns(self):
        return {
            "opds": OpdsFeedExtension.OPDS_NS,
        }


class OpdsEntryExtension(BaseEntryExtension):
    def __init__(self):
        super().__init__()
        self.__image = None
        self.__thumbnail = None
        self.__links = []

    def image(self, href: str, mimetype: str, **kwargs):
        self.__image = {
            "rel": "http://opds-spec.org/image",
            "href": href,
            "type": mimetype,
        }
        self.__image.update(kwargs)

    def thumbnail(self, href: str, mimetype: str, **kwargs):
        self.__thumbnail = {
            "rel": "http://opds-spec.org/image/thumbnail",
            "href": href,
            "type": mimetype,
        }
        self.__thumbnail.update(kwargs)

    def acquisition(self, href: str, mimetype: str, **kwargs):
        link = {
            "rel": "http://opds-spec.org/acquisition",
            "href": href,
            "type": mimetype,
        }
        link.update(kwargs)
        self.__links.append(link)

    def subsection(self, href: str, mimetype: str, **kwargs):
        link = {
            "rel": "subsection",
            "href": href,
            "type": mimetype,
        }
        link.update(kwargs)
        self.__links.append(link)

    def extend_atom(self, entry):
        if self.__image:
            xml_elem("link", entry, attrib=self.__image)

        if self.__thumbnail:
            xml_elem("link", entry, attrib=self.__thumbnail)

        for acq in self.__links:
            xml_elem("link", entry, attrib=acq)
        return entry


def add_opds_endpoints(
    routes: web.RouteTableDef, service_registry: ServiceRegistry, endpoint: str
):
    async def get_person_name(person_id: str):
        metadata = await service_registry.get_person_metadata_by_id(person_id)
        if metadata:
            return metadata.name
        return None

    async def add_book_entry(
        request: web.Request,
        fg: FeedGenerator,
        book: BookMetadata
    ):
        entry = fg.add_entry(order='append')
        opds_ext: OpdsEntryExtension = entry.opds

        title = await get_book_title(service_registry, book)
        entry.title(title)
        entry.id(f"{request.host}{endpoint}/opds/books/{book.book_id}")
        for person_id in book.writers:
            name = await get_person_name(person_id)
            if name:
                entry.author(name=name)

        for person_id in (
            book.artists
            + book.letterers
            + book.colorists
            + book.editors
            + book.translators
            + book.contributors
        ):
            name = await get_person_name(person_id)
            if name:
                entry.contributor(name=name)

        if book.description:
            entry.summary(book.description)
            entry.content(book.description)
        else:
            for series_entry in book.series:
                if series_entry.primary:
                    series_metadata = await service_registry.\
                        get_series_metadata_by_id(series_entry.series_id)
                    if series_metadata:
                        entry.summary(series_metadata.description)
                        entry.content(series_metadata.description)
            else:
                entry.content(book.title)

        entry.rights(book.rights)
        opds_ext.image(
            href=f"{endpoint}/book/{url_quote(book.book_id)}/cover",
            mimetype="image/jpg",
        )
        opds_ext.thumbnail(
            href=f"{endpoint}/book/{url_quote(book.book_id)}/cover",
            mimetype="image/jpg",
        )

        formats = set()
        for book_source in book.source_metadata.values():
            opds_ext.acquisition(
                href=(
                    f"{endpoint}/source/{url_quote(book_source.source_key)}/download"
                ),
                mimetype=f"{book_source.source_type.get_mimetype()}",
                title=f"Download {book_source.source_type.name}",
            )
            formats.add(book_source.source_type)

        for book_source in book.source_metadata.values():
            for transcode in await service_registry.get_book_transcode_targets(
                book_source.source_type
            ):
                if transcode not in formats:
                    opds_ext.acquisition(
                        href=(
                            f"{endpoint}/source/{url_quote(book_source.source_key)}"
                            f"/transcode/{url_quote(transcode.name)}"
                        ),
                        mimetype=f"{transcode.get_mimetype()}",
                        title=f"Download {transcode.name} (Transcoded)",
                    )
                    formats.add(transcode)

    @routes.get(endpoint + "/opds/root.xml")
    async def opds_root(request: web.Request):
        logger.debug("Serving OPDS root to %s", str(request.remote))
        activate_profile_from_request(request)

        fg = FeedGenerator()
        fg.register_extension(
            "opds",
            extension_class_feed=OpdsFeedExtension,
            extension_class_entry=OpdsEntryExtension,
        )
        fg.load_extension("dc")
        fg.id(f"{request.host}{endpoint}/opds")
        fg.title("Alexandria Books")
        fg.link(
            rel="self",
            href=(
                f"{endpoint}/opds/root.xml"
            ),
            type="application/atom+xml;profile=opds-catalog;kind=navigation",
        )
        fg.link(
            rel="start",
            href=f"{endpoint}/opds/root.xml",
            type="application/atom+xml;profile=opds-catalog;kind=navigation",
            title="Root",
        )
        fg.link(
            rel="search",
            href=f"{endpoint}/opensearch/search.xml",
            type="application/opensearchdescription+xml",
            title="Search",
        )

        all_books_entry = fg.add_entry(order='append')
        all_books_opds_ext: OpdsEntryExtension = all_books_entry.opds
        all_books_entry.id(f"{request.host}{endpoint}/opds/books")
        all_books_entry.title("All Books")
        all_books_entry.content("A listing of all available books.", type="text")
        all_books_opds_ext.subsection(
            href=f"{endpoint}/opds/books.xml",
            mimetype="application/atom+xml;profile=opds-catalog;kind=acquisition"
        )

        all_people_entry = fg.add_entry(order='append')
        all_people_opds_ext: OpdsEntryExtension = all_people_entry.opds
        all_people_entry.id(f"{request.host}{endpoint}/opds/people")
        all_people_entry.title("By Person")
        all_people_entry.content("A listing of all available people.", type="text")
        all_people_opds_ext.subsection(
            href=f"{endpoint}/opds/people.xml",
            mimetype="application/atom+xml;profile=opds-catalog;kind=navigation"
        )

        all_series_entry = fg.add_entry(order='append')
        all_series_opds_ext: OpdsEntryExtension = all_series_entry.opds
        all_series_entry.id(f"{request.host}{endpoint}/opds/series")
        all_series_entry.title("By Series")
        all_series_entry.content("A listing of all available series.", type="text")
        all_series_opds_ext.subsection(
            href=f"{endpoint}/opds/series.xml",
            mimetype="application/atom+xml;profile=opds-catalog;kind=navigation"
        )

        response = web.Response(status=200)
        response.content_type = "application/atom+xml;profile=opds-catalog"
        response.charset = "UTF-8"
        response.body = fg.atom_str(pretty=True)
        return response

    @routes.get(endpoint + "/opds/books.xml")
    async def opds_books(request: web.Request):
        logger.debug("Serving OPDS books to %s", str(request.remote))

        activate_profile_from_request(request)
        terms = request.query.get("terms", "").strip()
        offset_str = request.query.get("offset") or "0"
        limit_str = request.query.get("limit") or "100"
        person_id = request.query.get("person_id")
        series_id = request.query.get("series_id")

        try:
            offset = max(0, int(offset_str))
        except ValueError:
            logger.warning(f"Could not parse offset: {offset_str}")
            offset = 0

        try:
            limit = max(1, int(limit_str))
        except ValueError:
            logger.warning(f"Could not parse limit: {limit_str}")
            limit = 100

        self_url = (
            f"{endpoint}/opds/books.xml?terms={ url_quote(terms) }"
            f"&offset={ offset }&limit={ limit }"
        )

        prev_page_offset: typing.Optional[int] = max(0, offset - limit)
        next_page_offset: typing.Optional[int] = offset + limit

        if prev_page_offset == offset:
            prev_page_offset = None

        person_ids: typing.Optional[typing.List[str]] = None
        if person_id:
            person_ids = [person_id]
            self_url += f"&person_id={person_id}"

        series_ids: typing.Optional[typing.List[str]] = None
        if series_id:
            series_ids = [series_id]
            self_url += f"&series_id={series_id}"

        search_results = await service_registry.search_books(
            terms,
            person_ids=person_ids,
            series_ids=series_ids
        )
        if offset:
            await search_results.skip(offset)

        books: typing.List[BookMetadata] = []
        async for book in search_results.results():
            if len(books) >= limit:
                break
            books.append(book)

        if not books or not search_results.has_next:
            next_page_offset = None

        fg = FeedGenerator()
        fg.register_extension(
            "opds",
            extension_class_feed=OpdsFeedExtension,
            extension_class_entry=OpdsEntryExtension,
        )
        fg.load_extension("dc")
        fg.id(f"{request.host}{endpoint}/opds/books")
        fg.title("Alexandria Books")
        fg.link(
            rel="self",
            href=self_url,
            type="application/atom+xml;profile=opds-catalog;kind=acquisition",
        )
        fg.link(
            rel="start",
            href=f"{endpoint}/opds/root.xml",
            type="application/atom+xml;profile=opds-catalog;kind=acquisition",
            title="Root",
        )
        fg.link(
            rel="search",
            href=f"{endpoint}/opensearch/search.xml",
            type="application/opensearchdescription+xml",
            title="Search",
        )
        if prev_page_offset is not None:
            fg.link(
                rel="prev",
                href=(
                    f"{endpoint}/opds/books.xml?terms={ url_quote(terms) }"
                    f"&offset={ prev_page_offset }&limit={ limit }"
                ),
                type="application/atom+xml;profile=opds-catalog;kind=acquisition",
                title="Previous Page",
            )
        if next_page_offset is not None:
            fg.link(
                rel="next",
                href=(
                    f"{endpoint}/opds/books.xml?terms={ url_quote(terms) }"
                    f"&offset={ next_page_offset }&limit={ limit }"
                ),
                type="application/atom+xml;profile=opds-catalog;kind=acquisition",
                title="Next Page",
            )

        for book in books:
            await add_book_entry(request, fg, book)

        response = web.Response(status=200)
        response.content_type = "application/atom+xml;profile=opds-catalog"
        response.charset = "UTF-8"
        response.body = fg.atom_str(pretty=True)
        return response

    @routes.get(endpoint + "/opds/people.xml")
    async def opds_people(request: web.Request):
        logger.debug("Serving OPDS people to %s", str(request.remote))

        activate_profile_from_request(request)
        terms = request.query.get("terms", "").strip()
        offset_str = request.query.get("offset") or "0"
        limit_str = request.query.get("limit") or "100"

        try:
            offset = max(0, int(offset_str))
        except ValueError:
            logger.warning(f"Could not parse offset: {offset_str}")
            offset = 0

        try:
            limit = max(1, int(limit_str))
        except ValueError:
            logger.warning(f"Could not parse limit: {limit_str}")
            limit = 100

        prev_page_offset: typing.Optional[int] = max(0, offset - limit)
        next_page_offset: typing.Optional[int] = offset + limit

        if prev_page_offset == offset:
            prev_page_offset = None

        search_results = await service_registry.search_people(
            terms
        )
        if offset:
            await search_results.skip(offset)

        people: typing.List[PersonMetadata] = []
        async for person in search_results.results():
            if len(people) >= limit:
                break
            people.append(person)

        if not people or not search_results.has_next:
            next_page_offset = None

        fg = FeedGenerator()
        fg.register_extension(
            "opds",
            extension_class_feed=OpdsFeedExtension,
            extension_class_entry=OpdsEntryExtension,
        )
        fg.load_extension("dc")
        fg.id(f"{request.host}{endpoint}/opds/people")
        fg.title("Alexandria Books")
        fg.link(
            rel="self",
            href=(
                f"{endpoint}/opds/people.xml?terms={ url_quote(terms) }"
                f"&offset={ offset }&limit={ limit }"
            ),
            type="application/atom+xml;profile=opds-catalog;kind=navigation",
        )
        fg.link(
            rel="start",
            href=f"{endpoint}/opds/root.xml",
            type="application/atom+xml;profile=opds-catalog;kind=navigation",
            title="Root",
        )
        fg.link(
            rel="search",
            href=f"{endpoint}/opensearch/search.xml",
            type="application/opensearchdescription+xml",
            title="Search",
        )
        if prev_page_offset is not None:
            fg.link(
                rel="prev",
                href=(
                    f"{endpoint}/opds/people.xml?terms={ url_quote(terms) }"
                    f"&offset={ prev_page_offset }&limit={ limit }"
                ),
                type="application/atom+xml;profile=opds-catalog;kind=navigation",
                title="Previous Page",
            )
        if next_page_offset is not None:
            fg.link(
                rel="next",
                href=(
                    f"{endpoint}/opds/people.xml?terms={ url_quote(terms) }"
                    f"&offset={ next_page_offset }&limit={ limit }"
                ),
                type="application/atom+xml;profile=opds-catalog;kind=navigation",
                title="Next Page",
            )

        for person_metadata in people:
            entry = fg.add_entry(order='append')
            entry_opds_ext: OpdsEntryExtension = entry.opds
            entry.id(f"{request.host}{endpoint}/opds/people/{person_metadata.person_id}")
            entry.title(person_metadata.name)
            if person_metadata.bio:
                entry.content(
                    person_metadata.name + " - " + person_metadata.bio,
                    type="text"
                )
            else:
                entry.content(
                    person_metadata.name,
                    type="text"
                )

            entry_opds_ext.subsection(
                href=f"{endpoint}/opds/books.xml?person_id={person_metadata.person_id}",
                mimetype="application/atom+xml;profile=opds-catalog;kind=acquisition"
            )

        response = web.Response(status=200)
        response.content_type = "application/atom+xml;profile=opds-catalog"
        response.charset = "UTF-8"
        response.body = fg.atom_str(pretty=True)
        return response

    @routes.get(endpoint + "/opds/series.xml")
    async def opds_series(request: web.Request):
        logger.debug("Serving OPDS series to %s", str(request.remote))

        activate_profile_from_request(request)
        terms = request.query.get("terms", "").strip()
        offset_str = request.query.get("offset") or "0"
        limit_str = request.query.get("limit") or "100"

        try:
            offset = max(0, int(offset_str))
        except ValueError:
            logger.warning(f"Could not parse offset: {offset_str}")
            offset = 0

        try:
            limit = max(1, int(limit_str))
        except ValueError:
            logger.warning(f"Could not parse limit: {limit_str}")
            limit = 100

        prev_page_offset: typing.Optional[int] = max(0, offset - limit)
        next_page_offset: typing.Optional[int] = offset + limit

        if prev_page_offset == offset:
            prev_page_offset = None

        search_results = await service_registry.search_series(
            terms
        )
        if offset:
            await search_results.skip(offset)

        series: typing.List[SeriesMetadata] = []
        async for series_entry in search_results.results():
            if len(series) >= limit:
                break
            series.append(series_entry)

        if not series:
            next_page_offset = None

        fg = FeedGenerator()
        fg.register_extension(
            "opds",
            extension_class_feed=OpdsFeedExtension,
            extension_class_entry=OpdsEntryExtension,
        )
        fg.load_extension("dc")
        fg.id(f"{request.host}{endpoint}/opds/series")
        fg.title("Alexandria Books")
        fg.link(
            rel="self",
            href=(
                f"{endpoint}/opds/series.xml?terms={ url_quote(terms) }"
                f"&offset={ offset }&limit={ limit }"
            ),
            type="application/atom+xml;profile=opds-catalog;kind=navigation",
        )
        fg.link(
            rel="start",
            href=f"{endpoint}/opds/root.xml",
            type="application/atom+xml;profile=opds-catalog;kind=navigation",
            title="Root",
        )
        fg.link(
            rel="search",
            href=f"{endpoint}/opensearch/search.xml",
            type="application/opensearchdescription+xml",
            title="Search",
        )
        if prev_page_offset is not None:
            fg.link(
                rel="prev",
                href=(
                    f"{endpoint}/opds/series.xml?terms={ url_quote(terms) }"
                    f"&offset={ prev_page_offset }&limit={ limit }"
                ),
                type="application/atom+xml;profile=opds-catalog;kind=navigation",
                title="Previous Page",
            )
        if next_page_offset is not None:
            fg.link(
                rel="next",
                href=(
                    f"{endpoint}/opds/series.xml?terms={ url_quote(terms) }"
                    f"&offset={ next_page_offset }&limit={ limit }"
                ),
                type="application/atom+xml;profile=opds-catalog;kind=navigation",
                title="Next Page",
            )

        for series_metadata in series:
            entry = fg.add_entry(order='append')
            entry_opds_ext: OpdsEntryExtension = entry.opds
            entry.id(f"{request.host}{endpoint}/opds/series/{series_metadata.series_id}")
            entry.title(series_metadata.title)
            if series_metadata.description:
                entry.content(
                    series_metadata.title + " - " + series_metadata.description,
                    type="text"
                )
            else:
                entry.content(
                    series_metadata.title,
                    type="text"
                )

            entry_opds_ext.subsection(
                href=f"{endpoint}/opds/books.xml?series_id={series_metadata.series_id}",
                mimetype="application/atom+xml;profile=opds-catalog;kind=acquisition"
            )

        response = web.Response(status=200)
        response.content_type = "application/atom+xml;profile=opds-catalog"
        response.charset = "UTF-8"
        response.body = fg.atom_str(pretty=True)
        return response
