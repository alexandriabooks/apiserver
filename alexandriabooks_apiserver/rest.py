import typing
import logging
from aiohttp import web
from alexandriabooks_core import config, model
from alexandriabooks_core.services import ServiceRegistry
from alexandriabooks_core.utils import copy_streams, get_valid_filename
from .utils import activate_profile_from_request, get_book_title, url_unquote

logger = logging.getLogger(__name__)


def iter_stream(stream, chunk_size=4096):
    while True:
        chunk = stream.read(chunk_size)
        if not chunk:
            break
        yield chunk


def get_image_transcoder_options(request: web.Request) -> typing.Dict[str, typing.Any]:
    image_w_str = request.query.get("image_w", None)
    image_h_str = request.query.get("image_h", None)

    max_image_w_str = config.get("web.rest.image.max_w")
    max_image_h_str = config.get("web.rest.image.max_h")
    image_type_str = config.get("web.rest.image.type")
    greyscale = config.get_bool("web.rest.image.greyscale")

    image_type: typing.Optional[model.ImageType] = None
    if image_type_str:
        image_type = model.ImageType.to_image_type(image_type_str)

    if not image_type:
        image_type = model.ImageType.JPEG

    image_w: typing.Optional[int] = None
    image_h: typing.Optional[int] = None
    max_image_w: typing.Optional[int] = None
    max_image_h: typing.Optional[int] = None

    if image_w_str:
        try:
            image_w = int(image_w_str)
        except ValueError:
            logger.warning(f"Could not parse {image_w_str} as number")

    if image_h_str:
        try:
            image_h = int(image_h_str)
        except ValueError:
            logger.warning(f"Could not parse {image_h_str} as number")
            image_h = None

    if max_image_w_str:
        try:
            max_image_w = int(max_image_w_str)
        except ValueError:
            logger.warning(f"Could not parse {max_image_w_str} as number")

    if max_image_h_str:
        try:
            max_image_h = int(max_image_h_str)
        except ValueError:
            logger.warning(f"Could not parse {max_image_h_str} as number")

    return {
        "desired_size": (image_w, image_h),
        "max_size": (max_image_w, max_image_h),
        "desired_type": image_type,
        "greyscale": greyscale,
    }


def image_options_to_cache_key(options: typing.Dict[str, typing.Any]) -> str:
    return (
        f"{options['desired_size'][0] or '0'}"
        f"x{options['desired_size'][1] or '0'}"
        f"_{options['max_size'][0] or '0'}"
        f"x{options['max_size'][1] or '0'}"
        f"_{'BW' if options['greyscale'] else'C'}"
    )


async def respond_with_stream(request: web.Request, stream, headers=None):
    headers = headers or {}
    response = web.StreamResponse(status=200, reason="OK", headers=headers)

    await response.prepare(request)
    for chunk in iter_stream(stream):
        await response.write(chunk)
    return response


def add_cover_endpoint(
    routes: web.RouteTableDef, service_registry: ServiceRegistry, endpoint: str
):
    @routes.get(endpoint + "/book/{book_id}/cover")
    async def download_book_cover(request: web.Request):
        activate_profile_from_request(request)
        book_id = url_unquote(request.match_info["book_id"])
        book: typing.Optional[model.Book] = await service_registry.get_book_by_id(book_id)
        image_transcoder_options = get_image_transcoder_options(request)

        if not book:
            return web.HTTPNotFound(text=f"No book with id: {book_id}")

        logger.debug(
            "Serving book cover for book %s to %s",
            book_id,
            str(request.remote)
        )

        try:
            async def get_cover(out):
                front_cover = book.get_metadata().front_cover
                if not front_cover:
                    return False
                with front_cover.as_stream() as stream:
                    await service_registry.transcode_image(
                        src=stream, out_stream=out, **image_transcoder_options
                    )
                return True

            cache_name = (
                f"api_server/book/{book_id}/"
                + "cover_"
                + image_options_to_cache_key(image_transcoder_options)
                + f".{image_transcoder_options['desired_type'].ext()}"
            )

            cover_stream = await service_registry.get_stream(cache_name, get_cover)
            if cover_stream:
                return await respond_with_stream(
                    request,
                    cover_stream,
                    headers={
                        "Content-Type": image_transcoder_options[
                            "desired_type"
                        ].get_mimetype()
                    },
                )
        except Exception as exp:
            logger.error("Could not retrieve book cover", exc_info=exp)

        raise web.HTTPNotFound(text="No cover available.")

    @routes.get(endpoint + "/source/{source_key}/cover")
    async def download_source_cover(request: web.Request):
        activate_profile_from_request(request)
        source_key = request.match_info["source_key"]
        image_transcoder_options = get_image_transcoder_options(request)

        logger.debug(
            "Serving book source cover for source key %s to %s",
            source_key,
            str(request.remote)
        )

        try:

            async def get_cover(out):
                book = await service_registry.get_book_source_by_key(source_key)
                if not book:
                    return False
                front_cover = book.get_metadata().front_cover
                if not front_cover:
                    return False
                with front_cover.as_stream() as stream:
                    await service_registry.transcode_image(
                        src=stream, out_stream=out, **image_transcoder_options
                    )
                return True

            cache_name = (
                f"api_server/source/{source_key}/"
                + "cover_"
                + image_options_to_cache_key(image_transcoder_options)
                + f".{image_transcoder_options['desired_type'].ext()}"
            )

            cover_stream = await service_registry.get_stream(cache_name, get_cover)
            if cover_stream:
                return await respond_with_stream(
                    request,
                    cover_stream,
                    headers={
                        "Content-Type": image_transcoder_options[
                            "desired_type"
                        ].get_mimetype()
                    },
                )
        except Exception as exp:
            logger.error("Could not retrieve source cover", exc_info=exp)

        raise web.HTTPNotFound(text="No cover available.")


def add_download_endpoint(
    routes: web.RouteTableDef, service_registry: ServiceRegistry, endpoint: str
):
    @routes.get(endpoint + "/source/{source_key}/download")
    async def download_book(request: web.Request):
        activate_profile_from_request(request)
        source_key = url_unquote(request.match_info["source_key"])
        book_source: typing.Optional[model.BookSource] = \
            await service_registry.get_book_source_by_key(
                source_key
            )

        if book_source is not None:
            logger.debug(
                "Serving book source for source key %s (%s) to %s",
                source_key,
                book_source.source_type,
                str(request.remote)
            )

            accessor = await book_source.get_accessor()
            source_type = book_source.source_type
            filename = await get_book_title(service_registry, book_source.get_metadata())
            if not filename:
                filename = accessor.get_filename()
            filename = get_valid_filename(filename) + "." + source_type.ext()
            stream = await accessor.get_stream()
            return await respond_with_stream(
                request,
                stream,
                headers={
                    "Content-Type": source_type.get_mimetype(),
                    "Content-Disposition": f'attachment; filename="{filename}"',
                },
            )

        raise web.HTTPNotFound()


def add_transcode_endpoint(
    routes: web.RouteTableDef, service_registry: ServiceRegistry, endpoint: str
):
    @routes.get(endpoint + "/source/{source_key}/transcode/{dest_type}")
    async def transcode_book(request: web.Request):

        profile_name = activate_profile_from_request(request)
        source_key = url_unquote(request.match_info["source_key"])
        dest_type_str = request.match_info["dest_type"]
        try:
            dest_type = model.SourceType[dest_type_str]
        except KeyError:
            raise web.HTTPBadRequest()

        image_transcoder_options = get_image_transcoder_options(request)

        book_source = await service_registry.get_book_source_by_key(source_key)
        if book_source is not None:
            logger.debug(
                "Transcoding book source %s from type %s to type %s for %s",
                source_key,
                book_source.source_type,
                dest_type,
                str(request.remote)
            )

            accessor = await book_source.get_accessor()

            filename = await get_book_title(service_registry, book_source.get_metadata())
            if not filename:
                filename = accessor.get_filename()[
                    : -(len(book_source.source_type.ext()) + 1)
                ]
            filename = get_valid_filename(filename) + "." + dest_type.ext()

            async def get_transcode(out):
                transcode_stream = await service_registry.transcode_book(
                    book_source.get_metadata(),
                    accessor,
                    dest_type,
                    image_transcoder_options=image_transcoder_options,
                )
                if transcode_stream is not None:
                    await copy_streams(transcode_stream, out)
                    return True

            cache_name = f"api_server/source/{source_key}/transcode/"
            if profile_name:
                cache_name += f"{profile_name}_"
            cache_name += (
                f"{accessor.get_filename()[:-(len(book_source.source_type.ext()) + 1)]}"
            )
            cache_name += image_options_to_cache_key(image_transcoder_options)
            cache_name += "." + dest_type.ext()

            stream = await service_registry.get_stream(cache_name, get_transcode)

            if stream:
                return await respond_with_stream(
                    request,
                    stream,
                    headers={
                        "Content-Type": dest_type.get_mimetype(),
                        "Content-Disposition": f'attachment; filename="{filename}"',
                    },
                )
        raise web.HTTPNotFound()
