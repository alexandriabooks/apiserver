import logging
import typing
import os.path
from aiohttp import web
import aiohttp_jinja2
from alexandriabooks_core.services import ServiceRegistry
from alexandriabooks_core import config, model
from .utils import activate_profile_from_request, get_book_title, url_quote

logger = logging.getLogger(__name__)


async def get_transcode_targets(
    service_registry: ServiceRegistry,
    book: model.BookMetadata
) -> typing.List[
    typing.Tuple[
        str,
        typing.List[model.SourceType]
    ]
]:
    transcode_targets = []
    for source in book.source_metadata.values():
        target = await service_registry.get_book_transcode_targets(source.source_type)
        transcode_targets.append((source.source_key, target))
    return transcode_targets


async def get_writer_names(
    service_registry: ServiceRegistry,
    book: model.BookMetadata
) -> typing.List[str]:
    names = []
    for writer_id in book.writers:
        person = await service_registry.get_person_metadata_by_id(writer_id)
        if person:
            names.append(person.name)
    return names


def add_interface_endpoints(
    routes: web.RouteTableDef, service_registry: ServiceRegistry, endpoint: str
):
    @routes.get("/lofi")
    @aiohttp_jinja2.template("lofi.html")
    async def lofi_index(request: web.Request):
        logger.debug("Serving lofi interface to %s", str(request.remote))

        profile_name = activate_profile_from_request(request)
        config.set_profile(profile_name)
        terms = request.query.get("terms", "").strip()
        offset_str = request.query.get("offset") or "0"
        limit_str = request.query.get("limit", config.get("web.lofi.page_size")) or "100"
        layout = request.query.get("layout", config.get("web.lofi.layout"))

        supported_types_str = config.get(
                "web.lofi.supported_types",
        ) or ",".join([source_type.name for source_type in model.SourceType])

        supported_types = {
            model.SourceType[source_type_name]
            for source_type_name in supported_types_str.split(",")
        }

        try:
            offset = max(0, int(offset_str))
        except ValueError:
            logger.warning(f"Could not parse offset: {offset_str}")
            offset = 0

        try:
            limit = max(1, int(limit_str))
        except ValueError:
            logger.warning(f"Could not parse limit: {limit_str}")
            limit = 100

        prev_page_offset: typing.Optional[int] = max(0, offset - limit)
        next_page_offset: typing.Optional[int] = offset + limit

        if prev_page_offset == offset:
            prev_page_offset = None

        search_results = await service_registry.search_books(
            terms
        )
        if offset:
            await search_results.skip(offset)

        books: typing.List[
            typing.Tuple[
                model.BookMetadata,
                str,
                typing.List[str],
                typing.List[
                    typing.Tuple[
                        str,
                        typing.List[model.SourceType]
                    ]
                ]
            ]
        ] = []

        async for result in search_results.results():
            if len(books) >= limit:
                break
            books.append((
                result,
                await get_book_title(service_registry, result),
                await get_writer_names(service_registry, result),
                await get_transcode_targets(service_registry, result)
            ))

        # We searched for one extra book above, to see if we have a next page.
        if not books or not search_results.has_next:
            next_page_offset = None

        common_url_query = f"terms={url_quote(terms)}"
        common_url_info = ""
        if profile_name:
            common_url_query += f"&profile={profile_name}"
            common_url_info += f"profile={profile_name}"

        if layout:
            common_url_query += f"&layout={layout}"

        return {
            "request": request,
            "service_registry": service_registry,
            "endpoint": endpoint,
            "url_quote": url_quote,
            "books": books,
            "terms": terms,
            "offset": offset,
            "limit": limit,
            "prev_page_offset": prev_page_offset,
            "next_page_offset": next_page_offset,
            "profile_name": profile_name,
            "thumb_w": config.get("web.rest.image.thumb_w"),
            "thumb_h": config.get("web.rest.image.thumb_h"),
            "supported_types": supported_types,
            "common_url_query": common_url_query,
            "common_url_info": common_url_info,
            "layout": layout
        }

    hifi_path = config.get("web.hifi.path")
    if hifi_path:
        if os.path.isdir(hifi_path):
            hifi_path = os.path.abspath(hifi_path)
            logger.info("Enabling HiFi interface at %s", hifi_path)

            @routes.get("/hifi")
            async def hifi_index(request):
                return web.FileResponse(os.path.join(hifi_path, "index.html"))

            routes.static("/hifi", hifi_path)
        else:
            logger.warning("Disabling HiFi, path %s does not exist", hifi_path)
