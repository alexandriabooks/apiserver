import typing
import logging
import re
from urllib.parse import quote, unquote

from alexandriabooks_core import config, model
from alexandriabooks_core.services.registry import ServiceRegistry
from aiohttp import web

logger = logging.getLogger(__name__)


def activate_profile_from_request(request: web.Request) -> typing.Optional[str]:
    selected_profile_name: typing.Optional[str] = None

    agent_strings = config.get_prefix("web.profile_activation.agent_string")
    user_agent = request.headers.get('user-agent')
    if user_agent:
        for profile_name, agent_string_re in agent_strings.items():
            try:
                if re.match(agent_string_re, user_agent):
                    selected_profile_name = profile_name.lower()
                    break
            except Exception as e:
                logger.warning(f"Invalid regex: {agent_string_re}", exc_info=e)

    selected_profile_name = request.query.get("profile", selected_profile_name)

    return config.set_profile(selected_profile_name)


async def get_book_title(
    service_registry: ServiceRegistry,
    book: typing.Union[
        model.BookMetadata,
        model.BaseBookMetadata
    ]
) -> str:
    title = ""
    if book.title:
        title += book.title

    if book.subtitle:
        if title:
            title += " - "
        title += book.subtitle

    for series_entry in book.series:
        if series_entry.primary:
            if not title:
                series_metadata = await service_registry.get_series_metadata_by_id(
                    series_entry.series_id
                )
                if series_metadata and series_metadata.title:
                    title = series_metadata.title

            if series_entry.volume:
                if title:
                    title += ", "
                title += "Volume: " + ", ".join(series_entry.volume)

            if series_entry.issue:
                if title:
                    title += ", "
                title += "Issue: " + ",".join(series_entry.issue)

            break

    return title


def url_quote(value):
    return quote(value, '')


def url_unquote(value):
    return unquote(value, '')
