import typing
import re

from ..utils import url_quote

from alexandriabooks_core.services import ServiceRegistry
from alexandriabooks_core.model import (
    Tag,
    BookMetadata,
    PersonMetadata,
    SeriesMetadata,
    BaseBookMetadata,
    BaseSeriesMetadata,
    BasePersonMetadata,
    SourceProvidedBookMetadata,
    ScraperProvidedBookMetadata,
    SourceType
)

from graphql.type.definition import (
    GraphQLArgument, GraphQLField,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLList,
    GraphQLResolveInfo
)
from graphql.type.scalars import (
    GraphQLBoolean, GraphQLInt,
    GraphQLString,
    GraphQLID,
)

__CASE_CONVERTER__ = re.compile(r"([A-Z])")


class SearchResults(typing.NamedTuple):
    results: typing.List[typing.Any]
    next_cursor: str


def merge_dicts(
    source: typing.Dict[str, typing.Any],
    *others
) -> typing.Dict[str, typing.Any]:
    new_dict = source.copy()
    for other_dict in others:
        new_dict.update(other_dict)
    return new_dict


def snake_case_sorts(
    sort: typing.Optional[typing.List[str]] = None
) -> typing.Optional[typing.List[str]]:
    if not sort:
        return sort

    fixed = []
    for entry in sort:
        if not entry:
            continue
        spec = entry.split(":")
        spec[0] = __CASE_CONVERTER__.sub(r"_\1", spec[0]).lower()

        fixed.append(":".join(spec))

    return fixed


def resolve_tag_value(
    tag: typing.Optional[Tag],
    info: GraphQLResolveInfo
) -> typing.Optional[str]:
    if tag is None:
        return None
    value = tag.value
    if value is not None:
        return str(value)
    return None


def resolve_tags(
    metadata: typing.Union[BaseBookMetadata, BaseSeriesMetadata, None],
    info: GraphQLResolveInfo
) -> typing.List[Tag]:
    if not metadata:
        return []

    return [
        tag
        for tag in (metadata.tags or [])
        if not tag.key.startswith("_")
    ]


async def resolve_book_source_metadata(
    book: typing.Optional[BookMetadata],
    info: GraphQLResolveInfo,
    keys: typing.Optional[typing.List[str]] = None
) -> typing.Optional[typing.Iterable[SourceProvidedBookMetadata]]:
    if not book:
        return None
    if not keys:
        return book.source_metadata.values()
    else:
        key_set = set(keys)
        return [
            value
            for key, value in book.source_metadata.items()
            if key in key_set
        ]


async def resolve_book_scraper_metadata(
    book: typing.Optional[BookMetadata],
    info: GraphQLResolveInfo,
    keys: typing.Optional[typing.List[str]] = None
) -> typing.Optional[typing.Iterable[ScraperProvidedBookMetadata]]:
    if not book:
        return None
    if not keys:
        return book.scraper_metadata.values()
    else:
        key_set = set(keys)
        return [
            value
            for key, value in book.scraper_metadata.items()
            if key in key_set
        ]


async def resolve_people(
    obj: typing.Optional[BasePersonMetadata],
    info: GraphQLResolveInfo
):
    service_registry: ServiceRegistry = info.context["service_registry"]
    ret = []
    for person_id in getattr(obj, info.field_name):
        person = await service_registry.get_person_metadata_by_id(person_id)
        if person:
            ret.append(person)
    return ret


async def resolve_book(
    obj: typing.Optional[typing.Any],
    info: GraphQLResolveInfo,
    id: str
) -> typing.Optional[BookMetadata]:
    service_registry: ServiceRegistry = info.context["service_registry"]
    book = await service_registry.get_book_by_id(id)
    if book:
        return book.get_metadata()
    return None


async def resolve_person(
    obj: typing.Optional[typing.Any],
    info: GraphQLResolveInfo,
    id: str
) -> typing.Optional[PersonMetadata]:
    service_registry: ServiceRegistry = info.context["service_registry"]
    return await service_registry.get_person_metadata_by_id(id)


async def resolve_series(
    obj: typing.Optional[typing.Any],
    info: GraphQLResolveInfo,
    id: str
) -> typing.Optional[SeriesMetadata]:
    service_registry: ServiceRegistry = info.context["service_registry"]
    return await service_registry.get_series_metadata_by_id(id)


async def resolve_search_books(
    obj: typing.Optional[typing.Any],
    info: GraphQLResolveInfo,
    terms: typing.Optional[str] = None,
    sort: typing.Optional[typing.List[str]] = None,
    offset: typing.Optional[int] = None,
    limit: typing.Optional[int] = None,
    sourceTypes: typing.Optional[typing.List[str]] = None,
    seriesIds: typing.Optional[typing.List[str]] = None,
    personIds: typing.Optional[typing.List[str]] = None,
    includeSourceless: typing.Optional[bool] = None,
    cursor: typing.Optional[str] = None
) -> SearchResults:
    service_registry: ServiceRegistry = info.context["service_registry"]

    if includeSourceless is None:
        includeSourceless = False

    if limit is None:
        limit = 100

    if cursor:
        results = await service_registry.continue_search_books(cursor)
    else:
        terms = terms or ""

        parsed_source_types: typing.Optional[typing.List[SourceType]]
        if sourceTypes:
            parsed_source_types = [
                SourceType.to_source_type(source_type_str)
                for source_type_str in sourceTypes
            ]
        else:
            parsed_source_types = None

        results = await service_registry.search_books(
            terms=terms,
            sort=snake_case_sorts(sort),
            source_types=parsed_source_types,
            series_ids=seriesIds,
            person_ids=personIds,
            include_sourceless=includeSourceless
        )

    books: typing.List[BookMetadata] = []
    if offset:
        await results.skip(offset)
    async for entry in results.results():
        books.append(entry)
        if len(books) >= limit:
            break

    return SearchResults(
        books,
        results.cursor
    )


async def resolve_search_people(
    obj: typing.Optional[typing.Any],
    info: GraphQLResolveInfo,
    terms: typing.Optional[str] = None,
    sort: typing.Optional[typing.List[str]] = None,
    offset: typing.Optional[int] = None,
    limit: typing.Optional[int] = None,
    includeSourceless: typing.Optional[bool] = None,
    cursor: typing.Optional[str] = None
) -> SearchResults:
    service_registry: ServiceRegistry = info.context["service_registry"]

    if includeSourceless is None:
        includeSourceless = False

    if limit is None:
        limit = 100

    if cursor:
        results = await service_registry.continue_search_people(cursor)
    else:
        terms = terms or ""

        results = await service_registry.search_people(
            terms=terms,
            sort=snake_case_sorts(sort),
            include_sourceless=includeSourceless
        )

    people: typing.List[PersonMetadata] = []
    if offset:
        await results.skip(offset)
    async for entry in results.results():
        people.append(entry)
        if len(people) >= limit:
            break

    return SearchResults(
        people,
        results.cursor
    )


async def resolve_search_series(
    obj: typing.Optional[typing.Any],
    info: GraphQLResolveInfo,
    terms: typing.Optional[str] = None,
    sort: typing.Optional[typing.List[str]] = None,
    offset: typing.Optional[int] = None,
    limit: typing.Optional[int] = None,
    includeSourceless: typing.Optional[bool] = None,
    cursor: typing.Optional[str] = None
) -> SearchResults:
    service_registry: ServiceRegistry = info.context["service_registry"]

    if includeSourceless is None:
        includeSourceless = False

    if limit is None:
        limit = 100

    if cursor:
        results = await service_registry.continue_search_series(cursor)
    else:
        terms = terms or ""

        results = await service_registry.search_series(
            terms=terms,
            sort=snake_case_sorts(sort),
            include_sourceless=includeSourceless
        )

    series: typing.List[SeriesMetadata] = []
    if offset:
        await results.skip(offset)
    async for entry in results.results():
        series.append(entry)
        if len(series) >= limit:
            break

    return SearchResults(
        series,
        results.cursor
    )


tag_type = GraphQLObjectType(
    "BookTag",
    {
        "id": GraphQLField(
            GraphQLID,
            resolve=lambda tag, info: tag.key + "-" + str(tag.value)
        ),
        "key": GraphQLField(
            GraphQLString,
            resolve=lambda tag, info: tag.key
        ),
        "value": GraphQLField(
            GraphQLString,
            resolve=resolve_tag_value
        )
    }
)

series_type = GraphQLObjectType(
    "Series",
    {
        "id": GraphQLField(
            GraphQLID,
            resolve=lambda metadata, info: metadata.series_id
        ),
        "title": GraphQLField(
            GraphQLString,
            resolve=lambda metadata, info: metadata.title
        ),
        "description": GraphQLField(
            GraphQLString,
            resolve=lambda metadata, info: metadata.description
        ),
        "tags": GraphQLField(
            GraphQLList(GraphQLNonNull(tag_type)),
            resolve=resolve_tags
        ),
        "poster_url": GraphQLField(
            GraphQLString,
            resolve=lambda metadata, info: None
        ),
        "banner_url": GraphQLField(
            GraphQLString,
            resolve=lambda metadata, info: None
        )
    }
)

series_result_type = GraphQLObjectType(
    "SeriesResults",
    {
        "items": GraphQLField(
            GraphQLList(series_type),
            resolve=lambda search_results, info: search_results.results
        ),
        "next_cursor": GraphQLField(
            GraphQLNonNull(GraphQLString),
            resolve=lambda search_results, info: search_results.next_cursor
        )
    }
)

series_entry_type = GraphQLObjectType(
    "SeriesEntry",
    {
        "id": GraphQLField(
            GraphQLID,
            resolve=lambda metadata, info: metadata.series_id
        ),
        "details": GraphQLField(
            series_type,
            resolve=lambda metadata, info:
            info.context["service_registry"].get_series_metadata_by_id(metadata.series_id)
        ),
        "volume": GraphQLField(
            GraphQLList(GraphQLString),
            resolve=lambda metadata, info: metadata.volume
        ),
        "issue": GraphQLField(
            GraphQLList(GraphQLString),
            resolve=lambda metadata, info: metadata.issue
        ),
        "primary": GraphQLField(
            GraphQLBoolean,
            resolve=lambda metadata, info: metadata.primary
        )
    }
)

reading_list_entry_type = GraphQLObjectType(
    "ReadingListEntry",
    {
        "id": GraphQLField(
            GraphQLID,
            resolve=lambda metadata, info: metadata.series_id
        ),
        "details": GraphQLField(
            series_type,
            resolve=lambda metadata, info:
            info.context["service_registry"].get_series_metadata_by_id(metadata.series_id)
        ),
        "index": GraphQLField(
            GraphQLInt,
            resolve=lambda metadata, info: metadata.index
        )
    }
)

person_type = GraphQLObjectType(
    "Person",
    {
        "id": GraphQLField(
            GraphQLID,
            resolve=lambda metadata, info: metadata.person_id
        ),
        "name": GraphQLField(
            GraphQLString,
            resolve=lambda metadata, info: metadata.name
        ),
        "firstName": GraphQLField(
            GraphQLString,
            resolve=lambda metadata, info: metadata.first_name
        ),
        "middleNames": GraphQLField(
            GraphQLString,
            resolve=lambda metadata, info: metadata.middle_names
        ),
        "lastName": GraphQLField(
            GraphQLString,
            resolve=lambda metadata, info: metadata.last_name
        ),
        "bio": GraphQLField(
            GraphQLString,
            resolve=lambda metadata, info: metadata.bio
        ),
        "tags": GraphQLField(
            GraphQLList(GraphQLNonNull(tag_type)),
            resolve=resolve_tags
        ),
        "poster_url": GraphQLField(
            GraphQLString,
            resolve=lambda metadata, info: None
        )
    }
)

person_result_type = GraphQLObjectType(
    "PersonResults",
    {
        "items": GraphQLField(
            GraphQLList(person_type),
            resolve=lambda search_results, info: search_results.results
        ),
        "next_cursor": GraphQLField(
            GraphQLNonNull(GraphQLString),
            resolve=lambda search_results, info: search_results.next_cursor
        )
    }
)

base_metadata_fields = {
    "title": GraphQLField(
        GraphQLString,
        resolve=lambda metadata, info: metadata.title
    ),
    "subtitle": GraphQLField(
        GraphQLString,
        resolve=lambda metadata, info: metadata.subtitle
    ),
    "publisher": GraphQLField(
        GraphQLString,
        resolve=lambda metadata, info: metadata.publisher
    ),
    "rights": GraphQLField(
        GraphQLString,
        resolve=lambda metadata, info: metadata.rights
    ),
    "publishDate": GraphQLField(
        GraphQLString,
        resolve=lambda metadata, info: metadata.publish_date
    ),
    "language": GraphQLField(
        GraphQLList(GraphQLNonNull(GraphQLString)),
        resolve=lambda metadata, info: metadata.language
    ),
    "country": GraphQLField(
        GraphQLString,
        resolve=lambda metadata, info: metadata.country
    ),
    "age": GraphQLField(
        GraphQLInt,
        resolve=lambda metadata, info: metadata.age
    ),
    "description": GraphQLField(
        GraphQLString,
        resolve=lambda metadata, info: metadata.description
    ),
    "genres": GraphQLField(
        GraphQLList(GraphQLNonNull(GraphQLString)),
        resolve=lambda metadata, info: metadata.genres
    ),
    "tags": GraphQLField(
        GraphQLList(GraphQLNonNull(tag_type)),
        resolve=resolve_tags
    ),
    "acquisitionDate": GraphQLField(
        GraphQLString,
        resolve=lambda metadata, info: metadata.acquisition_date
    ),
    "series": GraphQLField(
        GraphQLList(GraphQLNonNull(series_entry_type)),
        resolve=lambda metadata, info: metadata.series
    ),
    "readingLists": GraphQLField(
        GraphQLList(GraphQLNonNull(reading_list_entry_type)),
        resolve=lambda metadata, info: metadata.reading_lists
    ),
    "people": GraphQLField(
        GraphQLList(GraphQLNonNull(person_type)),
        resolve=resolve_people
    ),
    "writers": GraphQLField(
        GraphQLList(GraphQLNonNull(person_type)),
        resolve=resolve_people
    ),
    "artists": GraphQLField(
        GraphQLList(GraphQLNonNull(person_type)),
        resolve=resolve_people
    ),
    "coverartists": GraphQLField(
        GraphQLList(GraphQLNonNull(person_type)),
        resolve=resolve_people
    ),
    "letterers": GraphQLField(
        GraphQLList(GraphQLNonNull(person_type)),
        resolve=resolve_people
    ),
    "colorists": GraphQLField(
        GraphQLList(GraphQLNonNull(person_type)),
        resolve=resolve_people
    ),
    "inkers": GraphQLField(
        GraphQLList(GraphQLNonNull(person_type)),
        resolve=resolve_people
    ),
    "pencillers": GraphQLField(
        GraphQLList(GraphQLNonNull(person_type)),
        resolve=resolve_people
    ),
    "editors": GraphQLField(
        GraphQLList(GraphQLNonNull(person_type)),
        resolve=resolve_people
    ),
    "translators": GraphQLField(
        GraphQLList(GraphQLNonNull(person_type)),
        resolve=resolve_people
    ),
    "contributors": GraphQLField(
        GraphQLList(GraphQLNonNull(person_type)),
        resolve=resolve_people
    ),
}

user_metadata_type = GraphQLObjectType(
    "UserMetadata",
    merge_dicts(
        {
            "id": GraphQLField(
                GraphQLID,
                resolve=lambda source, info: source.book_id
            )
        },
        base_metadata_fields,
        {
            "coverUrl": GraphQLField(
                GraphQLString,
                resolve=lambda source, info:
                f"/api/user/{url_quote(source.book_id)}/cover"
            )
        }
    )
)

book_source_metadata_type = GraphQLObjectType(
    "BookSourceMetadata",
    merge_dicts(
        {
            "id": GraphQLField(
                GraphQLID,
                resolve=lambda source, info: source.source_key
            )
        },
        base_metadata_fields,
        {
            "sourceType": GraphQLField(
                GraphQLString,
                resolve=lambda source, info: source.source_type.name
            ),
            "coverUrl": GraphQLField(
                GraphQLString,
                resolve=lambda source, info:
                f"/api/source/{url_quote(source.source_key)}/cover"
            ),
            "downloadUrl": GraphQLField(
                GraphQLString,
                resolve=lambda source, info:
                f"/api/source/{url_quote(source.source_key)}/download"
            )
        }
    )
)


book_scraper_metadata_type = GraphQLObjectType(
    "BookScraperMetadata",
    merge_dicts(
        {
            "id": GraphQLField(
                GraphQLID,
                resolve=lambda scraper, info: scraper.scraper_key
            )
        },
        base_metadata_fields,
        {
            "coverUrl": GraphQLField(
                GraphQLString,
                resolve=lambda scraper, info:
                f"/api/scraper/{url_quote(scraper.source_key)}/cover"
            )
        }
    )
)

book_transcodes_type = GraphQLObjectType(
    "BookTranscodes",
    {
        "id": GraphQLField(
            GraphQLID,
            resolve=lambda transcode, info: transcode["id"]
        ),
        "originalSourceId": GraphQLField(
            GraphQLNonNull(GraphQLString),
            resolve=lambda transcode, info: transcode["original_source_id"]
        ),
        "sourceType": GraphQLField(
            GraphQLNonNull(GraphQLString),
            resolve=lambda transcode, info: transcode["source_type"]
        ),
        "downloadUrl": GraphQLField(
            GraphQLNonNull(GraphQLString),
            resolve=lambda transcode, info: transcode["download_url"]
        ),
    }
)


async def resolve_book_transcodes(
    book: BookMetadata,
    info: GraphQLResolveInfo
):
    service_registry: ServiceRegistry = info.context["service_registry"]

    transcode_targets = []
    for source in book.source_metadata.values():
        for target in await service_registry.get_book_transcode_targets(
            source.source_type
        ):
            transcode_targets.append(
                {
                    "id": source.source_key + "-" + target.name,
                    "original_source_id": source.source_key,
                    "source_type": target.name,
                    "download_url":
                    f"/api/source/{url_quote(source.source_key)}/transcode/{target.name}"
                }
            )
    return transcode_targets


book_type = GraphQLObjectType(
    "Book",
    merge_dicts(
        {
            "id": GraphQLField(
                GraphQLID,
                resolve=lambda book, info: book.book_id
            )
        },
        base_metadata_fields,
        {
            "coverUrl": GraphQLField(
                GraphQLString,
                resolve=lambda book, info: f"/api/book/{url_quote(book.book_id)}/cover"
            ),
            "userMetadata": GraphQLField(
                GraphQLNonNull(user_metadata_type),
                resolve=lambda book, info: book.user_metadata
            ),
            "sources": GraphQLField(
                GraphQLList(GraphQLNonNull(book_source_metadata_type)),
                args={
                    "keys": GraphQLArgument(GraphQLList(GraphQLNonNull(GraphQLString)))
                },
                resolve=resolve_book_source_metadata
            ),
            "scrapers": GraphQLField(
                GraphQLList(GraphQLNonNull(book_scraper_metadata_type)),
                args={
                    "keys": GraphQLArgument(GraphQLList(GraphQLNonNull(GraphQLString)))
                },
                resolve=resolve_book_scraper_metadata
            ),
            "transcodes": GraphQLField(
                GraphQLList(GraphQLNonNull(book_transcodes_type)),
                resolve=resolve_book_transcodes
            )
        }
    )
)

book_result_type = GraphQLObjectType(
    "BookResults",
    {
        "items": GraphQLField(
            GraphQLList(book_type),
            resolve=lambda search_results, info: search_results.results
        ),
        "next_cursor": GraphQLField(
            GraphQLNonNull(GraphQLString),
            resolve=lambda search_results, info: search_results.next_cursor
        )
    }
)

query_type = GraphQLObjectType(
    "Query",
    {
        "search": GraphQLField(
            book_result_type,
            args={
                "terms": GraphQLArgument(GraphQLString),
                "sort": GraphQLArgument(
                    GraphQLList(GraphQLNonNull(GraphQLString))
                ),
                "offset": GraphQLArgument(GraphQLInt),
                "limit": GraphQLArgument(GraphQLInt),
                "sourceTypes": GraphQLArgument(
                    GraphQLList(GraphQLNonNull(GraphQLString))
                ),
                "seriesIds": GraphQLArgument(
                    GraphQLList(GraphQLNonNull(GraphQLString))
                ),
                "personIds": GraphQLArgument(
                    GraphQLList(GraphQLNonNull(GraphQLString))
                ),
                "includeSourceless": GraphQLArgument(
                    GraphQLBoolean,
                    default_value=False
                ),
                "cursor": GraphQLArgument(GraphQLString)
            },
            resolve=resolve_search_books
        ),
        "search_people": GraphQLField(
            person_result_type,
            args={
                "terms": GraphQLArgument(GraphQLString),
                "sort": GraphQLArgument(
                    GraphQLList(GraphQLNonNull(GraphQLString))
                ),
                "offset": GraphQLArgument(GraphQLInt),
                "limit": GraphQLArgument(GraphQLInt),
                "includeSourceless": GraphQLArgument(
                    GraphQLBoolean,
                    default_value=False
                ),
                "cursor": GraphQLArgument(GraphQLString)
            },
            resolve=resolve_search_people
        ),
        "search_series": GraphQLField(
            series_result_type,
            args={
                "terms": GraphQLArgument(GraphQLString),
                "sort": GraphQLArgument(
                    GraphQLList(GraphQLNonNull(GraphQLString))
                ),
                "offset": GraphQLArgument(GraphQLInt),
                "limit": GraphQLArgument(GraphQLInt),
                "includeSourceless": GraphQLArgument(
                    GraphQLBoolean,
                    default_value=False
                ),
                "cursor": GraphQLArgument(GraphQLString)
            },
            resolve=resolve_search_series
        ),
        "book": GraphQLField(
            book_type,
            args={
                "id": GraphQLArgument(GraphQLNonNull(GraphQLID))
            },
            resolve=resolve_book
        ),
        "person": GraphQLField(
            person_type,
            args={
                "id": GraphQLArgument(GraphQLNonNull(GraphQLID))
            },
            resolve=resolve_person
        ),
        "series": GraphQLField(
            series_type,
            args={
                "id": GraphQLArgument(GraphQLNonNull(GraphQLID))
            },
            resolve=resolve_series
        )
    },
)
